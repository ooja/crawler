// Starts the crawler
// The crawler runs the manager which is responsible for
// starting crawling behaviours and cordinating the crawlers
package main

import (
    "bitbucket.org/ooja/crawler/manager"
    lgr "bitbucket.org/ooja/crawler/logger"
    "gopkg.in/alecthomas/kingpin.v1"
    "bitbucket.org/ooja/crawler/params"
    conns "bitbucket.org/ooja/crawler/connections"
    "os"
    "os/signal"
    "syscall"
)

var (
    parameters params.Params
    mongoConnStr = kingpin.Flag("mongo_server", "MongoDB server connection string").Default("localhost").String()
    numWorkers = kingpin.Flag("num_workers", "Number of workers").Default("1").Int()
    redisHost = kingpin.Flag("redis_host", "Redis hostname").Default("localhost").String()
    redisPort = kingpin.Flag("redis_port", "Redis port").Default("6379").Int()
    redisConnPoolSize = kingpin.Flag("redis_pool_size", "Redis pool size").Default("5").Int()
    mongoDBName = kingpin.Flag("mongo_dn", "MongoDB database name").Default("ooja").String()
    visitedListingsColName = kingpin.Flag("mongo_col_vl", "MongoDB collection for visited listings").Default("visited_listings").String()
    getDelayDur = kingpin.Flag("get_delay", "Sleep duration after each listing download").Default("5").Int()
    crawlStateChannel = kingpin.Flag("crawl_state_chan", "Crawl state channel name").Default("crawl_state").String()
    listingHTMLStoreName = kingpin.Flag("html_store_name", "Listings html store name").Default("listings_html").String()
    catCrawlDepthLimit = kingpin.Flag("cat_depth_limit", "Category depth limit").Default("0").Int()
    friutlessGiveUpDeptLimit = kingpin.Flag("giveup_depth_limt", "Friutless crawl give up depth limit").Default("0").Int()
)

func init(){
    kingpin.Version("0.0.1")
    kingpin.Parse()
}

func main() {
    
    lgr.GetLogger().Info("Crawler has started")
    
    // pack parameters in to a container
    parameters := params.Params {
        NumWorkers: *numWorkers,
        RedisHost: *redisHost,
        RedisPort: *redisPort,
        RedisConnPoolSize: *redisConnPoolSize,
        MongoDBName: *mongoDBName,
        VisitedListingColName: *visitedListingsColName,
        GetDelayDur: *getDelayDur,
        CrawlStateChannel: *crawlStateChannel,
        ListingHTMLStoreName: *listingHTMLStoreName,
        CatCrawlDepthLimit: *catCrawlDepthLimit,
        FruitlessGiveUpDepthLimit: *friutlessGiveUpDeptLimit,
    }

    // wait channel crawler's lifecycle
    end := make(chan bool)

    lgr.GetLogger().Info("connecting to mongodb")
    if conn := conns.ConnectToMongoDB(mongoConnStr); conn != nil {
        defer conn.Close()
    }

    lgr.GetLogger().Info("connecting to redis")
    redisCon := conns.ConnectToRedis(&parameters)
    defer redisCon.Close()

    // start manager
    mngr := manager.Manager{ &parameters }
    mngr.Start(end);

    // listen for system signals
    signalChannel := make(chan os.Signal, 2)
    signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)
    go func() {
        sig := <-signalChannel
        switch sig {
        case os.Interrupt, syscall.SIGTERM:
            lgr.GetLogger().Info("cleaning up and exiting")
            conns.GetRedisClient().Close()
            conns.GetMongoDBConn().Close()
            end <- true
        }
    }()

    <-end
}
