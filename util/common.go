package util

import (
	"crypto/sha1"
	"encoding/hex"
	"regexp"
	"errors"
	"net/url"
)

func ToSHA1(v string) string {
	h := sha1.New()
    h.Write([]byte(v))
    hx := h.Sum(nil)
    return hex.EncodeToString(hx)
}

func IsURL(url string) bool {
	rx := "(ftp|http|https)://(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(/|/([\\w#!:.?+=&%@!\\-\\/]))?"
	match, _ := regexp.MatchString(rx, url)
	return match
}

func RelURLToAbs(urlStr, host string) (string, error) {
	u, err := url.Parse(urlStr)
	if err != nil {
		return "", err
	}

	if !u.IsAbs() {

		hostURL, err2 := url.Parse(host)
		if err2 != nil {
			return "", errors.New("error parsing host url: " + err.Error())
		}

		if hostURL.Host != "" {
			u.Host = hostURL.Host
		}

		if hostURL.Scheme != "" {
			u.Scheme = hostURL.Scheme
		}

		return u.String(), nil


	} else {
		return urlStr, nil
	}

	return "", err
}