package worker

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"bitbucket.org/ooja/crawler/util"
)

func TestShouldBeValidURL (t *testing.T) {
	url := "http://jumia.com.ng"
	assert := assert.New(t)
	assert.Equal(util.IsURL(url), true, "should be true")
}

func TestRelURLToAbs(t *testing.T) {
	absUrl := "help/step-one"
	fullUrl, err := util.RelURLToAbs(absUrl, "http://google.com.ng")
	assert := assert.New(t)
	assert.Nil(err)
	assert.Equal(fullUrl, "http://google.com.ng/help/step-one", "must be equal")
}

func TestAttachQueryStrings(t *testing.T) {

	assert := assert.New(t)
	w := Worker{}

	urlData := []string{ "http://google.com", "http://google.com?a=2" }
	urlQuery := map[string]map[string]string{
		"listing": map[string]string{
			"q": "book",
		},
	}

	w.AttachQueryStrings(urlData, "listing", urlQuery)
	assert.Equal(urlData[0], "http://google.com?q=book", "must match")
	assert.Equal(urlData[1], "http://google.com?a=2&q=book", "must match")
}

func TestAttachQueryStrings2(t *testing.T) {

	assert := assert.New(t)
	w := Worker{}

	urlQuery := map[string]map[string]string{
		"listing": map[string]string{
			"q": "book",
		},
	}

	urlData := map[string]struct{}{
		"http://google.com": struct{}{},
		"http://google.com?a=2": struct{}{},
	}
	
	w.AttachQueryStrings(urlData, "listing", urlQuery)
	_, found := urlData["http://google.com?q=book"]
	assert.Equal(found, true)
	_, found = urlData["http://google.com?a=2&q=book"]
	assert.Equal(found, true)
}