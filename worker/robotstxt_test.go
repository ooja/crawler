package worker

import (
  "testing"
  "github.com/stretchr/testify/assert"
  "github.com/temoto/robotstxt-go"
  "bitbucket.org/ooja/crawler/fetcher"
)

func TestRobotsTXT(t *testing.T) {
    resp, err := fetcher.FetchURL("https://s3-eu-west-1.amazonaws.com/ooja-others/robots.txt")
    assert.Nil(t, err)
    txt, err := resp.Body.ToString()
    assert.Nil(t, err)
    robots, err := robotstxt.FromStatusAndBytes(200, []byte(txt))
    assert.Nil(t, err)
    allowed := robots.TestAgent("http://www.konga.com/acer-aspire-s7-393-intel-core-i7-8gb-256gb-ssd-13-3-inch-windows-8-1-ultrabook-1575552", "*")
    assert.Equal(t, allowed, true, "they should be true")
}

func TestRobotsTXTShouldNotMatch(t *testing.T) {
    resp, err := fetcher.FetchURL("https://s3-eu-west-1.amazonaws.com/ooja-others/robots.txt")
    assert.Nil(t, err)
    txt, err := resp.Body.ToString()
    assert.Nil(t, err)
    robots, err := robotstxt.FromStatusAndBytes(200, []byte(txt))
    assert.Nil(t, err)
    allowed := robots.TestAgent("http://www.konga.com/notebooks?limit=16&p=2&sm=1", "*")
    assert.Equal(t, allowed, false, "they should be false")
}
