// A worker crawls a store downloading categories and listings
// It must first request for a store from the url manager
package worker

import (
    lgr "bitbucket.org/ooja/crawler/logger"
    conns "bitbucket.org/ooja/crawler/connections"
    "bitbucket.org/ooja/crawler/model"
    "bitbucket.org/ooja/crawler/fetcher"
    "time"
    "errors"
    "encoding/json"
    "github.com/PuerkitoBio/goquery"
    "strings"
    "github.com/PuerkitoBio/purell"
    "bitbucket.org/ooja/crawler/params"
    "bytes"
    "strconv"
    "gopkg.in/mgo.v2/bson"
    "gopkg.in/redis.v2"
    "github.com/temoto/robotstxt-go"
    "bitbucket.org/ooja/crawler/util"
    "bitbucket.org/ooja/common"
    "net/url"
)

type Category struct {
    Id string
    Url string
}

type Worker struct {
    Id string
    Parameters *params.Params
    curStore *common.Store
}

// start worker setup
func (w *Worker) Start() {
    lgr.GetLogger().Info("crawler(", w.Id, ") has started")
    w.Do()
}

// get store from redis store_queue
func (w *Worker) getStore() string {
    return conns.GetRedisClient().SPop("store_queue").Val()
}

// get store data
func (w *Worker) getStoreData(storeId string) string {
    return conns.GetRedisClient().HGet("store_queue_data", storeId).Val()
}

// validate specific data fields in store object
func (w *Worker) validateStoreFields(store *common.Store) int {

    // selector validation
    if store.Selectors == nil {
        return MISSING_SELECTORS
    }

    // must category selector
    if store.Selectors["category"] == "" {
        return MISSING_CATEGORY_SELECTOR
    }

    // must provide listing selector
    if store.Selectors["listing"] == "" {
        return MISSING_LISTING_SELECTOR
    }

    // must provide page selector
    if store.Selectors["page"] == "" {
        return MISSING_PAGE_SELECTOR
    }

    return 1
}

// Get a remote content
// will retry if status code is >=500
func (w *Worker) Get(url string, contentType string, retryLimit int) (string, error) {

    retried := 0
    
    for {
        
        retried++

        if retried > 1 {
            lgr.GetLogger().Info(w.NameStr() + "refetching content. Trial count: ", retried)
        }

        // fetch the category sitemap url
        resp, err := fetcher.FetchURL(url)

        // error occured
        if nil != err {
            return "", errors.New("Error downloading from " + url + ":" + err.Error())
        
        } else {

            defer resp.Body.Close()
            body, err := resp.Body.ToString();

            docContentType := strings.ToLower(resp.Header.Get("Content-Type"))

            // charset must be utf-8
            if !strings.Contains(docContentType, "utf-8") {
                return "", errors.New(w.NameStr() + " charset(" + docContentType + ") is not supported")
            }

            // check content type
            if !strings.Contains(docContentType, contentType) {
                return "", errors.New(w.NameStr() + " doc content type (" + docContentType + ") is not supported. Expected " + contentType)
            }

            // allow retry if status is greater than 500
            if resp.StatusCode >= 500 {
                if retried > retryLimit {
                    return "", errors.New("Error reading from" + url + ":" + err.Error())
                }
                continue
            }

            if err == nil && resp.StatusCode == 200 {
                return body, nil

            } else {
                return "", errors.New("Error reading from" + url + ":" + err.Error())
            }
        }   
    }
}


// get a store robots.txt content
func (w *Worker) GetRobotTXT(store *common.Store) (string, int, error) {

    url := store.Domain + "/robots.txt"

    // fetch the category sitemap url
    resp, err := fetcher.FetchURL(url)
   
    if nil != err {
        return "", 0, errors.New("Error downloading from " + url + ":" + err.Error())
    
    } else {

        defer resp.Body.Close()
        body, err := resp.Body.ToString();
        if err != nil {
            return "", resp.StatusCode, errors.New("Unable to download robots.txt for store("+ store.Name+ ")")
        }

        return body, resp.StatusCode, err
    }
}

// recalls Do after sleeping for a while
func (w *Worker) reDo(errMsg string) {
    retrySec := 5
    lgr.GetLogger().Info(errMsg + "...fetching new store in ", retrySec, " seconds")
    time.Sleep(time.Duration(retrySec) * time.Second)
    w.Do()
}

// string name of worker
func (w *Worker) NameStr() string {
    return "worker(" + w.Id + "): "
}

// extract listing from a page using the store's 
// selector
func (w *Worker) GetListing (catPageHtml, selector, baseDomain string) (map[string]struct{}, error) {

    var listings = make(map[string]struct{})
    pageAsReader := bytes.NewReader([]byte(catPageHtml))
    doc, err := goquery.NewDocumentFromReader(pageAsReader)
    if err != nil {
        return listings, errors.New("error converting page to new goquery doc during listing extraction")
    }
    
    doc.Find(selector).Each(func(i int, s *goquery.Selection){
        if href, exists := s.Attr("href"); exists {
            if _, ok := listings[href]; !ok {
                // convert url to absolute if its relative
                href, err = util.RelURLToAbs(href, baseDomain)
                if err == nil {
                    if util.IsURL(href) {
                        if normHref, err := w.NormalizeUrl(href); err == nil {
                            listings[normHref] = struct{}{}
                        }
                    } else {
                        lgr.GetLogger().Error("url is not valid", href)
                    }
                } else {
                    lgr.GetLogger().Error("unable to parse url to absolute", href)
                }
            }
        }
    })

    return listings, nil
}

// filter out visited listing from a list of listings url
func (w *Worker) filterOutVisitedLinks(links map[string]struct{}) map[string]struct{} {
    unvisitedLinks := make(map[string]struct{})
    for l := range links {
        coll := conns.GetMongoDBConn().DB(w.Parameters.MongoDBName).C(w.Parameters.VisitedListingColName) 
        count, err := coll.Find(bson.M{ "url_hash": util.ToSHA1(l) }).Count()
        if err == nil && count == 0 {
            unvisitedLinks[l] = struct{}{}
        }
    }
    return unvisitedLinks
}

// extract page links from a page using the store's page
// selector
func (w *Worker) GetPageLinks (catPageHtml, selector, baseDomain string) (map[string]struct{}, error) {

    var pages = make(map[string]struct{})
    pageAsReader := bytes.NewReader([]byte(catPageHtml))
    doc, err := goquery.NewDocumentFromReader(pageAsReader)
    if err != nil {
        return pages, errors.New("error converting page to new goquery doc during page link extraction")
    }

    doc.Find(selector).Each(func(i int, s *goquery.Selection){
        if href, exists := s.Attr("href"); exists {
            if _, ok := pages[href]; !ok {
                // convert url to absolute if its relative
                href, err = util.RelURLToAbs(href, baseDomain)
                if err == nil {
                    if util.IsURL(href) {
                        if normHref, err := w.NormalizeUrl(href); err == nil {
                            pages[normHref] = struct{}{}
                        }
                    }
                } else {
                    lgr.GetLogger().Error("unable to parse url to absolute", href)
                }
            }
        }
    })

    return pages, nil
}

// normalize and return a url
func (w *Worker) NormalizeUrl(url string) (string, error) {
    return purell.NormalizeURLString(url, purell.FlagsSafe|purell.FlagAddWWW)
}

// save visited listing in mongodb database
func (w *Worker) PersistVisitedListing(url string, category string, storeId string, timestamp time.Time) {
    coll := conns.GetMongoDBConn().DB(w.Parameters.MongoDBName).C(w.Parameters.VisitedListingColName) 
    _, err := coll.Upsert(bson.M{"url_hash": util.ToSHA1(url) }, bson.M{ "$set": bson.M{ "url_hash": util.ToSHA1(url), "url": url, "category": util.ToSHA1(category), "store_id": storeId, "timestamp": timestamp }})
    if err != nil {
        lgr.GetLogger().Error(w.NameStr(), " unable to persist visited listing url. ", err.Error())
    }
}


// store newly download listing
func (w *Worker) StoreListing(listingDoc common.ListingDoc){
    b, err := json.Marshal(listingDoc)
    if err != nil {
        lgr.GetLogger().Error(w.NameStr(), "unable to parse listing doc to json. reason: ", err.Error())
    } 
    if err := conns.GetRedisClient().RPush(w.Parameters.ListingHTMLStoreName, string(b)).Err(); err != nil {
        lgr.GetLogger().Error(w.NameStr(), "unable to store listing doc")
    } else {
        // persist visited listing url
        w.PersistVisitedListing(listingDoc.ListingURL, listingDoc.PageURL, listingDoc.StoreId, time.Now())
    }
}


// set/update last crawl time of a store
func (w *Worker) setLastCrawlTime(store common.Store) {
    redisClient := conns.GetRedisClient()
    key := "lc_" + store.Id.Hex()
    curTimeStr := strconv.Itoa(int(time.Now().Unix()))
    if err := redisClient.Set(key, curTimeStr).Err(); err == nil {
        redisClient.Expire(key, 15 * time.Minute)
        lgr.GetLogger().Info(w.NameStr(),"updated last crawl time for store(", store.Name ,")")
    } else {
        lgr.GetLogger().Error(w.NameStr(), "unable to set last crawl time for store(", store.Name,")")
    }
}

// checks if a store is being processed by another worker by checking if
// its last crawl activity time is set
func (w *Worker) isBeingProcessed(storeId string) (bool, error) {
    redisClient := conns.GetRedisClient()
    key := "lc_" + storeId
    result, err := redisClient.Get(key).Result()
    if err == redis.Nil {
        if result != "" {
            return true, nil

        } else {
            return false, nil
        }
    } else {
        lgr.GetLogger().Error(err)
        return false, errors.New("unable to check store processing status")
    }
}

// publish message to url manager
func (w *Worker) publishMessageToURLManager(action string, payload string) {
    msg := `{ "action": "`+action+`", "payload":"`+payload+`" }`
    channel := w.Parameters.CrawlStateChannel
    err := conns.GetRedisClient().Publish(channel, msg).Err()
    if err == redis.Nil {
        lgr.GetLogger().Error(w.NameStr() + "unable to publish message (", msg, ") to channel("+channel+"): ", err.Error())
    } else {
        lgr.GetLogger().Info(w.NameStr() + "published new of completion of store to channel("+channel+")")
    }
}

// Extract category links by downloading store hompage and
// using category selector to pick category links
func (w *Worker) GetCategoryLinks(store *common.Store) ([]string, error) {

    links := []string{}

    // download store homepage
    hompageHTML, err := w.Get(store.Domain, "text/html", 3)
    if err != nil {
        return links, errors.New(w.NameStr() + "Could not fetch store(" + store.Name + ") hompage for category extraction. Reason:" + err.Error() + ". moving to next store")
    }

    // create goquery documents
    htmlAsReader := bytes.NewReader([]byte(hompageHTML))
    doc, err := goquery.NewDocumentFromReader(htmlAsReader)
    if err != nil {
        return links, errors.New("error converting homepage html to new goquery doc")
    }

    // using selector, find category links
    doc.Find(store.Selectors["category"].(string)).Each(func(i int, s *goquery.Selection){
        if href, exists := s.Attr("href"); exists {
            // convert relative url's to absolute
            if href, err = util.RelURLToAbs(href, store.Domain); err == nil {
                if util.IsURL(href) {
                    if normHref, err := w.NormalizeUrl(href); err == nil {
                        links = append(links, normHref)
                    }
                }
            } else {
                lgr.GetLogger().Error("unable to check and convert a category link ("+ href +") to absolute urls")
            }
        }
    })

    // add any provided url queries to category links 
    w.AttachQueryStrings(links, "category", store.URLQuery)

    return links, nil
}

// attach a set of query string to all urls in urlList
func (w *Worker) AttachQueryStrings(urlData interface{}, groupName string, urlQuery map[string]map[string]string) {
    if urlQuery[groupName] != nil {
        switch urls := urlData.(type){
        case []string:
            for i, u := range urls {
                u, err := url.Parse(u)
                if err != nil {
                    continue
                }
                q := u.Query()
                for key, val := range urlQuery[groupName] {
                    q.Set(key, val)
                }
                u.RawQuery = q.Encode()
                urls[i] = u.String()
            }
        case map[string]struct{}:
            for aUrl, val := range urls {
                u, err := url.Parse(aUrl)
                if err != nil {
                    continue
                }
                q := u.Query()
                for key, val := range urlQuery[groupName] {
                    q.Set(key, val)
                }
                u.RawQuery = q.Encode()
                delete(urls, aUrl)
                urls[u.String()] = val
            }
        }
    }
}

// begin new crawling tasks.
// fetch new store from queue then
// retreive categories of store. Download each category,
// extract listing and pagination links using the relevant store's 
// selectors. page by page, download each listing, send to html store 
func (w *Worker) Do() {

    lgr.GetLogger().Info("fetching store from queue")
    var robots robotstxt.RobotsData

    // get a store from store queue
    storeId := w.getStore()
    if storeId == "" {
        w.reDo(w.NameStr() + "no store in queue")
        return
    }

    // ensure store is not being processed
    // if it is, ignore it and refetch another
    if processing, err := w.isBeingProcessed(storeId); err != nil {
        w.reDo(w.NameStr() + err.Error())
        return
    } else {
        if processing {
            w.reDo(w.NameStr() + "store gotten is being processed by another worker")
            return
        }
    }
        
    // get store data
    storeDataJson := w.getStoreData(storeId)
    if storeDataJson == "" {
        w.reDo(w.NameStr() + "store(ID:" + storeId + ") data not retrieved(is empty)")
        return
    }
        
    // parse store data to json
    store, err := model.ParseFromJSONToStore(storeDataJson)
    if err != nil {
        w.reDo(w.NameStr() + "store(ID:" + storeId + "): could not parse json data to store object")
        return
    }

    // get robots.txt of store if enabled for store
    if !store.IgnoreRobotTXT {

        robotTxtContent, statusCode, err := w.GetRobotTXT(&store)
        if err != nil {
            w.reDo(w.NameStr() + "store(ID:" + storeId + "): error downloading robots.txt: " + err.Error())
            return
        }
        
        // create robots.txt parser object
        robots, err := robotstxt.FromStatusAndBytes(statusCode, []byte(robotTxtContent))
        if err != nil {
            lgr.GetLogger().Error("Error parsing robots.txt belonging to store(", store.Name ,"): Reason:", err.Error())
        }
    
        // ensure crawler has not been banned from crawling this store
        if allowed := robots.TestAgent(store.Domain, fetcher.UserAgent); !allowed {
            w.reDo(w.NameStr() + "store("+ store.Name +") robots.txt completely bans crawler from store")
            return 
        }
    }

    // set current working store
    w.curStore = &store
    
    // validate store data fields
    errCode := w.validateStoreFields(&store)
    if errCode != 1 {
        w.reDo(w.NameStr() + "store(ID:" + storeId + ") data failed validation. Reason: " + ERR(errCode))
        return   
    } 

    // get categories
    categories, err := w.GetCategoryLinks(&store)
    if err != nil {
        w.reDo(err.Error())
        return;
    }

    if len(categories) > 0 {
        lgr.GetLogger().Info(w.NameStr(), "extracted ", len(categories), " categories from store(Name:" + store.Name + ")")
    } else {
        lgr.GetLogger().Info(w.NameStr(), "no category found in homepage of store(Name:" + store.Name + ")")
    }

    // go through all categories
    for _, categoryURL := range categories {
        
        // pages queue contain all the pages to be visited in this category.
        // the category is always the first page
        pagesQueue := NewQueue()
        pagesQueue.Add(categoryURL)

        // holds the list of visited pages in these category
        visitedPages := make(map[string]struct{})

        // holds the number of pages downloaded
        pageProcessCount := 0

        // holds the number of fruitless pages downloaded
        // NB: a fruitless page is one that has no new listing
        fruitlessPageCount := 0

        for pagesQueue.Length() != 0 {

            // if page crawl depth limit is set and has been reached, stop process pages
            // of this category
            if w.Parameters.CatCrawlDepthLimit > 0 && pageProcessCount >= w.Parameters.CatCrawlDepthLimit {
                lgr.GetLogger().Info(w.NameStr(), "Category crawl depth limit reached for category")
                break
            }

            // if friutless depth limit is set and has been reached, stop processing pages
            // of this category
            if w.Parameters.FruitlessGiveUpDepthLimit > 0 && fruitlessPageCount >= w.Parameters.FruitlessGiveUpDepthLimit {
                lgr.GetLogger().Info(w.NameStr(), "Fruitless crawl depth limit reached for category")
                break
            }

            // get a page and remove it from the queue
            pageUrl := pagesQueue.Head()
            lgr.GetLogger().Info(w.NameStr(), "downloading new category page: ", pageUrl)

            // download page
            catPageHtml, err := w.Get(pageUrl.(string), "text/html", 3)
            if err != nil {
                lgr.GetLogger().Error(w.NameStr() + "store(", store.Name ,") category(", categoryURL ,") could not be downloaded. ", err.Error(), " | moving to next category")
                continue
            }

            // increment process page count
            pageProcessCount++

            // set last activity/crawl time in redis so other workers will 
            // not process this store while it's being processed by the current worker
            // and the url manager will not reinclude store into the queue
            go w.setLastCrawlTime(store)

            // add page to visited page list
            visitedPages[pageUrl.(string)] = struct{}{}

            // extract page links using page selector
            extractedPages, err := w.GetPageLinks(catPageHtml, store.Selectors["page"].(string), store.Domain)
            if err != nil {
                lgr.GetLogger().Error(w.NameStr() + err.Error())
                continue
            }

            // add any provided url query string data to extracted page links
            w.AttachQueryStrings(extractedPages, "page", store.URLQuery)

            lgr.GetLogger().Info(w.NameStr(), "extracted ", len(extractedPages), " category page(s) from current category page")

            // add newly extracted pages to the pages queue if not already included
            // also ensure extracted page has not been visited in these session
            for p, _ := range extractedPages {
                if exists := pagesQueue.Contains(p); !exists {
                    if _, exists := visitedPages[p]; !exists {
                        pagesQueue.Add(p)
                    }
                }
            }

            // extract listing from page
            extractedListings, err := w.GetListing(catPageHtml, store.Selectors["listing"].(string), store.Domain)
            if err != nil {
                lgr.GetLogger().Error(w.NameStr() + err.Error())
                continue
            }

            // add any provided url query string data to extracted listing links
            w.AttachQueryStrings(extractedListings, "listing", store.URLQuery)

            // filter out the listings that have been visited previously
            extractedListings = w.filterOutVisitedLinks(extractedListings)

            // increment fruitless page count if no listing was extracted from page
            if len(extractedListings) == 0 {
                fruitlessPageCount++
            } 

            lgr.GetLogger().Info(w.NameStr(), "Proceeding to download ", len(extractedListings), " listings of current category page")

            for listingUrl := range extractedListings {
                
                // sleep for a while before downloading listings
                time.Sleep(time.Duration(w.Parameters.GetDelayDur) * time.Second)

                lgr.GetLogger().Info(w.NameStr(), "downloading listing: ", listingUrl)

                // check if page url is allowed by store's robots.txt, if robots.txt checks is enabled for store
                if !store.IgnoreRobotTXT {
                    if allowed := robots.TestAgent(listingUrl, fetcher.UserAgent); !allowed {
                        lgr.GetLogger().Debug(w.NameStr(), "store(", store.Name, ") robots.txt bans download of " + listingUrl)
                        continue
                    }
                }

                listingPageHtml, err := w.Get(listingUrl, "text/html", 3); 
                if err != nil {
                    lgr.GetLogger().Error(w.NameStr(), "store(", store.Name ,"): Listing(", listingUrl ,") could not be downloaded. ", err.Error(), " | moving to next listing")
                    continue
                }

                // set last activity/crawl time in redis so other workers will 
                // not process this store while it's being processed by the current worker
                // and the url manager will not reinclude store into the queue
                go w.setLastCrawlTime(store)

                // create and save listing doc to html/doc store
                go w.StoreListing(common.ListingDoc{
                    Content: listingPageHtml,
                    CategoryURL: categoryURL,
                    ListingURL: listingUrl,
                    PageURL: pageUrl.(string),
                    StoreId: store.Id.Hex(),
                    Selectors: store.Selectors,
                })
            }
        }

        lgr.GetLogger().Info(w.NameStr(), "completed task on category ", categoryURL, ". moving to new category")
    }

    // inform urlmanager about store crawling being complete
    // so it can set store's next crawl time
    w.publishMessageToURLManager("crawl_complete", store.Id.Hex())

    // worker never dies. call reDo to start again
    w.reDo("completed task on store("+ store.Name +")")
}
