package model

import (
    "encoding/json"
    "bitbucket.org/ooja/common"
)

// Give a json data that represents a store, convert json
// to model.Store object
func ParseFromJSONToStore(d string) (common.Store, error) {
    s := &common.Store{}
    err := json.Unmarshal([]byte(d), &s); 
    return *s, err
}

