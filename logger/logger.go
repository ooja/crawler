package logger

import (
    "github.com/Sirupsen/logrus"
    "os"
)

var log = logrus.New()

func init() {
  //log.SetFormatter(&log.JSONFormatter{})
  log.Out = os.Stderr
  log.Level = logrus.DebugLevel
}

func GetLogger() *logrus.Logger {
    return log
}


