package params

type Params struct {
    NumWorkers int
    RedisHost string
    RedisPort int
    RedisConnPoolSize int
    MongoDBName string
    VisitedListingColName string
    GetDelayDur int
    CrawlStateChannel string
    ListingHTMLStoreName string
}
