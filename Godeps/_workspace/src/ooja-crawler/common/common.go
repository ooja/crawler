package common

import (
	"crypto/sha1"
	"encoding/hex"
)

func ToSHA1(v string) string {
	h := sha1.New()
    h.Write([]byte(v))
    hx := h.Sum(nil)
    return hex.EncodeToString(hx)
}
