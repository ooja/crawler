package fetcher

import (
    goreq "github.com/franela/goreq"
    "time"
)

var UserAgent string = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36"

// Fetch content a url
// Use a user agent and sets timeout and follows redirect
func FetchURL(url string) (*goreq.Response, error) {
    goreq.SetConnectTimeout(5 * time.Second)
    return goreq.Request{
        Uri: url,
        UserAgent: UserAgent,
        Timeout: 15 * time.Second,
        MaxRedirects: 3,
    }.Do()
}
