package worker

const (
	MISSING_CATEGORY_URL = -1
	MISSING_SELECTORS = -2
	MISSING_LISTING_SELECTOR = -3
	MISSING_PAGE_SELECTOR = -4
)

func ERR(code int) string {
	switch code {
		case MISSING_CATEGORY_URL:
			return "missing category url"
		case MISSING_SELECTORS:
			return "store is missing selectors"
		case MISSING_LISTING_SELECTOR:
			return "store listing selector is missing"
		case MISSING_PAGE_SELECTOR:
			return "store page selector is missing"
		default:
			return ""
	}
}
