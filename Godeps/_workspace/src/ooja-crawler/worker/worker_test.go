package worker

import (
  "testing"
  "github.com/stretchr/testify/assert"
)

var catData = `[
  {
    "url": "http://www.konga.com/laptops"

  },
  {
    "url": "http://www.konga.com/ultrabooks"
  },
  {
    "url": "http://www.konga.com/desktop-cpu"
  },
  {
    "url": "http://www.konga.com/lg-5287"
  },
  {
    "url": "http://www.konga.com/bags-luggages"
  }
]`

func TestParseCategories(t *testing.T) {
	categories, err := ParseCategories(catData)
	assert.Nil(t, err)
	assert.Equal(t, categories[0].Url, "http://www.konga.com/laptops", "first categories item should match")
	assert.Equal(t, len(categories), 5, "should contain 5 elements")
}
