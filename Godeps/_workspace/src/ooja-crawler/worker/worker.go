// A worker crawls a store downloading categories and listings
// It must first request for a store from the url manager
package worker

import (
    lgr "crawler/logger"
    conns "crawler/connections"
    "crawler/model"
    "crawler/fetcher"
    "time"
    "errors"
    "encoding/json"
    "github.com/PuerkitoBio/goquery"
    "strings"
    validator "github.com/asaskevich/govalidator"
    "github.com/PuerkitoBio/purell"
    "crawler/params"
    "bytes"
    "strconv"
    "gopkg.in/mgo.v2/bson"
    "gopkg.in/redis.v2"
    "crawler/common"
)

type Category struct {
    Id string
    Url string
}

type HTMLStoreItem struct {
    Content string
    ListingUrl string
    CategoryId string
    StoreId string
    Selectors map[string]string
}

type Worker struct {
    Id string
    Parameters *params.Params
    curStore *model.Store
}

// start worker setup
func (w *Worker) Start() {
    lgr.GetLogger().Info("crawler(", w.Id, ") has started")
    w.Do()
}

// get store from redis store_queue
func (w *Worker) getStore() string {
    return conns.GetRedisClient().SPop("store_queue").Val()
}

// get store data
func (w *Worker) getStoreData(storeId string) string {
    return conns.GetRedisClient().HGet("store_queue_data", storeId).Val()
}

// validate specific data fields in store object
func (w *Worker) validateStoreFields(store *model.Store) int {
    
    if store.CategoryURL == "" {
        return MISSING_CATEGORY_URL
    }

    // selector validation
    if store.Selectors == nil {
        return MISSING_SELECTORS
    }

    if store.Selectors["listing"] == "" {
        return MISSING_LISTING_SELECTOR
    }

    if store.Selectors["page"] == "" {
        return MISSING_PAGE_SELECTOR
    }

    return 1
}

// parse json aarray containing categories to 
// a slice of category object
func ParseCategories (categoryJSON string) ([]Category, error) {
    var categories []Category
    defer func() {
        recover()
    }()
    err := json.Unmarshal([]byte(categoryJSON), &categories)
    if err == nil {
        return categories, nil
    } else {
        return categories, errors.New("unable to parse json category data")
    }
}

// Get a remote content
// will retry if status code is >=500
func (w *Worker) Get(url string, contentType string, retryLimit int) (string, error) {

    retried := 0
    
    for {
        
        retried++

        if retried > 1 {
            lgr.GetLogger().Info(w.NameStr() + "refetching content. Trial count: ", retried)
        }

        // fetch the category sitemap url
        resp, err := fetcher.FetchURL(url)

        // error occured
        if nil != err {
            return "", errors.New("Error downloading from " + url + ":" + err.Error())
        
        } else {

            defer resp.Body.Close()
            body, err := resp.Body.ToString();

            docContentType := strings.ToLower(resp.Header.Get("Content-Type"))

            // charset must be utf-8
            if !strings.Contains(docContentType, "utf-8") {
                return "", errors.New(w.NameStr() + " charset(" + docContentType + ") is not supported")
            }

            // check content type
            if !strings.Contains(docContentType, contentType) {
                return "", errors.New(w.NameStr() + " doc content type (" + docContentType + ") is not supported. Expected " + contentType)
            }

            // allow retry if status is greater than 500
            if resp.StatusCode >= 500 {
                if retried > retryLimit {
                    return "", errors.New("Error reading from" + url + ":" + err.Error())
                }
                continue
            }

            if err == nil && resp.StatusCode == 200 {
                return body, nil

            } else {
                return "", errors.New("Error reading from" + url + ":" + err.Error())
            }
        }   
    }
}

// recalls Do after sleeping for a while
func (w *Worker) reDo(errMsg string) {
    retrySec := 5
    lgr.GetLogger().Info(errMsg + "...fetching new store in ", retrySec, " seconds")
    time.Sleep(time.Duration(retrySec) * time.Second)
    w.Do()
}

// string name of worker
func (w *Worker) NameStr() string {
    return "worker(" + w.Id + "): "
}

// extract listing from a page using the store's 
// selector
func (w *Worker) GetListing (catPageHtml string, selector string) (map[string]struct{}, error) {

    var listings = make(map[string]struct{})
    pageAsReader := bytes.NewReader([]byte(catPageHtml))
    doc, err := goquery.NewDocumentFromReader(pageAsReader)
    if err != nil {
        return listings, errors.New("error converting page to new goquery doc during listing extraction")
    }

    doc.Find(selector).Each(func(i int, s *goquery.Selection){
        if href, exists := s.Attr("href"); exists {
            if _, ok := listings[href]; !ok {
                if validator.IsURL(href) {
                    if normHref, err := w.NormalizeUrl(href); err == nil {
                        listings[normHref] = struct{}{}
                    }
                }
            }
        }
    })

    return listings, nil
}

// filter out visited listing from a list of listings url
func (w *Worker) filterOutVisitedLinks(links map[string]struct{}) map[string]struct{} {
    unvisitedLinks := make(map[string]struct{})
    for l := range links {
        coll := conns.GetMongoDBConn().DB(w.Parameters.MongoDBName).C(w.Parameters.VisitedListingColName) 
        count, err := coll.Find(bson.M{ "url_hash": common.ToSHA1(l) }).Count()
        if err == nil && count == 0 {
            unvisitedLinks[l] = struct{}{}
        }
    }
    return unvisitedLinks
}

// extract page links from a page using the store's page
// selector
func (w *Worker) GetPageLinks (catPageHtml string, selector string) (map[string]struct{}, error) {

    var pages = make(map[string]struct{})
    pageAsReader := bytes.NewReader([]byte(catPageHtml))
    doc, err := goquery.NewDocumentFromReader(pageAsReader)
    if err != nil {
        return pages, errors.New("error converting page to new goquery doc during page link extraction")
    }

    doc.Find(selector).Each(func(i int, s *goquery.Selection){
        if href, exists := s.Attr("href"); exists {
            if _, ok := pages[href]; !ok {
                if validator.IsURL(href) {
                    if normHref, err := w.NormalizeUrl(href); err == nil {
                        pages[normHref] = struct{}{}
                    }
                }
            }
        }
    })

    return pages, nil
}

// normalize and return a url
func (w *Worker) NormalizeUrl(url string) (string, error) {
    return purell.NormalizeURLString(url, purell.FlagsSafe|purell.FlagAddWWW)
}

// save visited listing in mongodb database
func (w *Worker) PersistVisitedListing(url string, category string, storeId string, timestamp time.Time) {
    coll := conns.GetMongoDBConn().DB(w.Parameters.MongoDBName).C(w.Parameters.VisitedListingColName) 
    _, err := coll.Upsert(bson.M{"url_hash": common.ToSHA1(url) }, bson.M{ "$set": bson.M{ "url_hash": common.ToSHA1(url), "url": url, "category": common.ToSHA1(category), "store_id": storeId, "timestamp": timestamp }})
    if err != nil {
        lgr.GetLogger().Error(w.NameStr(), " unable to persist visited listing url. ", err.Error())
    }
}


// store newly download listing
func (w *Worker) StoreListing(htmlStoreItem HTMLStoreItem){
    b, err := json.Marshal(htmlStoreItem)
    if err != nil {
        lgr.GetLogger().Error(w.NameStr(), "unable to parse listing content to json. reason: ", err.Error())
    } 
    if err := conns.GetRedisClient().RPush(w.Parameters.ListingHTMLStoreName, string(b)).Err(); err != nil {
        lgr.GetLogger().Error(w.NameStr(), "unable to store downloaded listing html")
    }
}


// set/update last crawl time of a store
func (w *Worker) setLastCrawlTime(store model.Store) {
    redisClient := conns.GetRedisClient()
    key := "lc_" + store.Id
    curTimeStr := strconv.Itoa(int(time.Now().Unix()))
    if err := redisClient.Set(key, curTimeStr).Err(); err == nil {
        redisClient.Expire(key, 15 * time.Minute)
        lgr.GetLogger().Info(w.NameStr(),"updated last crawl time for store(", store.Name ,")")
    } else {
        lgr.GetLogger().Error(w.NameStr(), "unable to set last crawl time for store(", store.Name,")")
    }
}

// checks if a store is being processed by another worker by checking if
// its last crawl activity time is set
func (w *Worker) isBeingProcessed(storeId string) (bool, error) {
    redisClient := conns.GetRedisClient()
    key := "lc_" + storeId
    result, err := redisClient.Get(key).Result()
    if err == redis.Nil {
        if result != "" {
            return true, nil

        } else {
            return false, nil
        }
    } else {
        return false, errors.New("unable to check store processing status. reason: " + err.Error())
    }
}

// publish message to url manager
func (w *Worker) publishMessageToURLManager(action string, payload string) {
    msg := `{ "action": "`+action+`", "payload":"`+payload+`" }`
    channel := w.Parameters.CrawlStateChannel
    err := conns.GetRedisClient().Publish(channel, msg).Err()
    if err == redis.Nil {
        lgr.GetLogger().Error(w.NameStr() + "unable to publish message (", msg, ") to channel("+channel+"): ", err.Error())
    } else {
        lgr.GetLogger().Info(w.NameStr() + "published new of completion of store to channel("+channel+")")
    }
}

// begin new crawling tasks.
// fetch new store from queue then
// retreive categories of store. Download each category,
// extract listing and pagination links using the relevant store's 
// selectors. page by page, download each listing, send to html store 
func (w *Worker) Do() {

    lgr.GetLogger().Info("fetching store from queue")

    // get a store from store queue
    storeId := w.getStore()
    if storeId == "" {
        w.reDo(w.NameStr() + "no store in queue")
        return
    }

    // ensure store is not being processed
    // if it is, ignore it and refetch another
    if processing, err := w.isBeingProcessed(storeId); err != nil {
        w.reDo(w.NameStr() + err.Error())
        return
    } else {
        if processing {
            w.reDo(w.NameStr() + "store gotten is being processed by another worker")
            return
        }
    }
        
    // get store data
    storeDataJson := w.getStoreData(storeId)
    if storeDataJson == "" {
        w.reDo(w.NameStr() + "store(ID:" + storeId + ") data not retrieved(is empty)")
        return
    }
        
    // parse store data to json
    store, err := model.ParseFromJSONToStore(storeDataJson)
    if err != nil {
        w.reDo(w.NameStr() + "store(ID:" + storeId + "): could not parse json data to store object")
        return
    }

    // set current working store
    w.curStore = &store

    // validate store data fields
    errCode := w.validateStoreFields(&store)
    if errCode != 1 {
        w.reDo(w.NameStr() + "store(ID:" + storeId + ") data failed validation. Reason: " + ERR(errCode))
        return   
    }

    // fetch category
    cat, err := w.Get(store.CategoryURL, "application/json", 3)
    if err != nil {
        w.reDo(w.NameStr() + "store(ID:" + storeId + "): unable to fetch category data: " + err.Error())
        return
    }

    // set last activity/crawl time in redis so other workers will 
    // not process this store while it's being processed by the current worker
    // and the url manager will not reinclude store into the queue
    go w.setLastCrawlTime(store)

    // parse category
    categories, err := ParseCategories(cat)
    if err != nil {
        w.reDo(w.NameStr() + "store(ID:" + storeId + "): unable to parse category data: " + err.Error())
    }  

    // go through all categories
    for _, category := range categories {
        
        // pages queue contain all the pages to be visited in this category.
        // the category is always the first page
        pagesQueue := NewQueue()
        pagesQueue.Add(category.Url)

        // holds the list of visited pages in these category
        visitedPages := make(map[string]struct{})

        for pagesQueue.Length() != 0 {

            // get a page and remove it from the queue
            pageUrl := pagesQueue.Head()
            lgr.GetLogger().Info(w.NameStr(), "downloading new category page: ", pageUrl)

            // download category link
            catPageHtml, err := w.Get(pageUrl.(string), "text/html", 3)
            if err != nil {
                lgr.GetLogger().Error(w.NameStr() + "store(", store.Name ,") category(", category.Url ,") could not be downloaded. ", err.Error(), " | moving to next category")
                continue
            }

            // set last activity/crawl time in redis so other workers will 
            // not process this store while it's being processed by the current worker
            // and the url manager will not reinclude store into the queue
            go w.setLastCrawlTime(store)

            // add page to visited page list
            visitedPages[pageUrl.(string)] = struct{}{}

            // extract page links
            extractedPages, err := w.GetPageLinks(catPageHtml, store.Selectors["page"])
            if err != nil {
                lgr.GetLogger().Error(w.NameStr() + err.Error())
                continue
            }
            lgr.GetLogger().Info(w.NameStr(), "extracted ", len(extractedPages), " category page(s) from current category page")

            // add newly extracted pages to the pages queue if not already included
            // also ensure extracted page has not been visited in these session
            for p, _ := range extractedPages {
                if exists := pagesQueue.Contains(p); !exists {
                    if _, exists := visitedPages[p]; !exists {
                        pagesQueue.Add(p)
                    }
                }
            }

            // extract listing from page
            extractedListings, err := w.GetListing(catPageHtml, store.Selectors["listing"])
            if err != nil {
                lgr.GetLogger().Error(w.NameStr() + err.Error())
                continue
            }
            lgr.GetLogger().Info(w.NameStr(), "extracted ", len(extractedListings), " listings from current category page")

            // filter out the listings that have been visited previously
            extractedListings = w.filterOutVisitedLinks(extractedListings)

            lgr.GetLogger().Info(w.NameStr(), "Proceeding to download ", len(extractedListings), " listings of current category page")

            for listingUrl := range extractedListings {
                
                // sleep for a while before downloading listings
                time.Sleep(time.Duration(w.Parameters.GetDelayDur) * time.Second)

                lgr.GetLogger().Info(w.NameStr(), "downloading listing: ", listingUrl)

                listingPageHtml, err := w.Get(listingUrl, "text/html", 3); 
                if err != nil {
                    lgr.GetLogger().Error(w.NameStr(), "store(", store.Name ,"): Listing(", listingUrl ,") could not be downloaded. ", err.Error(), " | moving to next listing")
                    continue
                }

                // set last activity/crawl time in redis so other workers will 
                // not process this store while it's being processed by the current worker
                // and the url manager will not reinclude store into the queue
                go w.setLastCrawlTime(store)

                // persist visited listing url
                go w.PersistVisitedListing(listingUrl, pageUrl.(string), store.Id, time.Now())

                // save listing to html store and make 
                go w.StoreListing(HTMLStoreItem{
                    Content: listingPageHtml,
                    CategoryId: category.Id,
                    ListingUrl: listingUrl,
                    StoreId: store.Id,
                    Selectors: store.Selectors,
                })
            }
        }

        lgr.GetLogger().Info(w.NameStr(), "completed task on category", category.Url, ". moving to new category")
    }

    // inform urlmanager about store crawling being complete
    // so it can set store's next crawl time
    w.publishMessageToURLManager("crawl_complete", store.Id)

    // worker never dies. call reDo to start again
    w.reDo("completed task on store("+ store.Name +")")
}
