package worker

import (
	mainQueue "github.com/eapache/queue"
)

// define queue struct
type queue struct {
	*mainQueue.Queue
	DataTrack map[interface{}]struct{}
}

// adds an entry into the queue
func (q *queue) Add(d interface{}){
	q.Queue.Add(d)
	q.DataTrack[d] = struct{}{}
}

// check if an iten is contained in the queue
func (q *queue) Contains(d interface{}) bool {
	if _, exists := q.DataTrack[d]; exists {
		return true
	}
	return false
}

// removes and returns the first item from the queue
func (q *queue) Head() interface{} {
	d := q.Queue.Peek()
	q.Queue.Remove()
	delete(q.DataTrack, d)
	return d
}

// instantiate a new queue
func NewQueue() *queue {
	q := &queue{ Queue: mainQueue.New()}
	q.DataTrack = make(map[interface{}]struct{})
	return q
}
