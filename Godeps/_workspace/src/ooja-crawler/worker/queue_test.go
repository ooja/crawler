package worker

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestAdd(t *testing.T) {
	queue := NewQueue()
	queue.Add("a")
	assert.Equal(t, queue.Length(), 1, "should contain the item")
}	

func TestHead(t *testing.T){
	queue := NewQueue()
	queue.Add("a")
	queue.Add("b")
	assert.Equal(t, queue.Head(), "a", "should return the first item")
}

func TestShouldContain(t *testing.T){
	queue := NewQueue()
	queue.Add("a")
	assert.Equal(t, queue.Contains("a"), true, "should contain the value")
}

func TestShouldNotContain(t *testing.T){
	queue := NewQueue()
	queue.Add("a")
	assert.Equal(t, queue.Contains("b"), false, "should not contain the value")
}
