// Manager starts the crawler behaviour
package manager

import (
    "crawler/params"
    lgr "crawler/logger"
    "github.com/satori/go.uuid"
    "crawler/worker"
)

type Manager struct {
    Parameters *params.Params
}

// Fires up goroutines as workers to crawl
// one store at a time
func (mngr *Manager) LaunchWorkers(){
    for i := 1; i <= mngr.Parameters.NumWorkers; i++ {
        id := uuid.NewV4().String()
        go func(){
            var worker = worker.Worker{ Id: id, Parameters: mngr.Parameters }
            worker.Start()
        }()
    }
}

// Starts the manager processes
func (mngr *Manager) Start(end chan bool) {

    lgr.GetLogger().Info("Launching ", mngr.Parameters.NumWorkers, " worker(s)")

    // launch workers
    mngr.LaunchWorkers()
}
