package model

import (
    "time"
    "encoding/json"
)

type Store struct {
    Id string
    Name string
    Domain string
    CategoryURL string
    LastCrawlTime string
    NextCrawlTime string
    Selectors map[string]string
}

type store struct {
    Id string
    Name string
    Domain string
    CategoryURL string
    LastCrawlTime time.Time
    NextCrawlTime time.Time
    Selectors map[string]string
}

// Give a json data that represents a store, convert json
// to model.Store object
func ParseFromJSONToStore(d string) (Store, error) {
    s := &store{}
    if err := json.Unmarshal([]byte(d), &s); err != nil {
        return Store{}, err
    } else {
        return Store{
            Id: s.Id,
            Name: s.Name,
            Domain: s.Domain,
            CategoryURL: s.CategoryURL,
            LastCrawlTime: s.LastCrawlTime.String(),
            NextCrawlTime: s.NextCrawlTime.String(),
            Selectors: s.Selectors,
        }, nil
    }
}

