package common

import (
	"gopkg.in/mgo.v2/bson"
)

type Store struct {
    Id bson.ObjectId `bson:"_id,omitempty"`
    Name string
    Domain string
    Categories []map[string]interface{}
    LastCrawlTime interface{}		`bson:"last_crawl_time"`
    NextCrawlTime interface{}		`bson:"next_crawl_time"`	
    Selectors map[string]interface{}
}
