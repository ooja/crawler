package common

type ListingDoc struct {
    Content string
    ListingUrl string
    PageUrl string
    CategoryURL string
    CategoryTags []interface{}
    StoreId string
    Selectors map[string]interface{}
}
