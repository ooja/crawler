package connections

import (
    "bitbucket.org/ooja/crawler/params"
    "strconv"
    "gopkg.in/redis.v2"
    "gopkg.in/mgo.v2"
)

var redisClient *redis.Client
var mongoDBConn *mgo.Session

// get redis client
func ConnectToRedis(parameters *params.Params) *redis.Client {
    client := redis.NewTCPClient(&redis.Options{
        Addr:     parameters.RedisHost + ":" + strconv.Itoa(parameters.RedisPort),
        Password: "", // no password set
        DB:       0,  // use default DB
        PoolSize: parameters.RedisConnPoolSize,
    })
    if _, err := client.Ping().Result(); err == nil {
        redisClient = client
        return client
    } else {
        panic("Unable to connect to redis: " + err.Error())
    }
}

// Connect to mongo database
func ConnectToMongoDB(mongoConnStr *string) *mgo.Session {
    session, err := mgo.Dial(*mongoConnStr)
    if err != nil {
        panic("Unable to connect to database using the connection string: " + *mongoConnStr)
    }
    mongoDBConn = session
    return session
}

// return redis client
func GetRedisClient() *redis.Client {
    return redisClient
}

// return mongodb connection
func GetMongoDBConn() *mgo.Session {
    return mongoDBConn
}
